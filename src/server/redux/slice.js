const redux = require('@reduxjs/toolkit');
const elastic = require('../elastic');

module.exports.crudSlicer = redux.createSlice({
  name: 'crud',
  initialState: {
      regions: []
  },
  reducers: {
    post: (state, obj) => {
      state.regions.push(obj);
    },
    clear: (state) => {
      state.regions = [];
    }
  }
});

module.exports.selectorRegions = state => state.regions;
module.exports.selectorRegionByName = (state, regName) => {
  var regByName = [];
  //console.log(state.regions);
  for(var i=0; i<state.regions.length; i++){
    const varName = state.regions[i].payload.denominazione_regione;
    if(varName === regName){
      regByName.push(state.regions[i].payload);
    }
  }
  return regByName;
}