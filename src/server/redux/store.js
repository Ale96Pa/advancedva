const redux = require('@reduxjs/toolkit');
const slice = require('./slice');

module.exports.store = redux.configureStore({
  reducer: slice.crudSlicer.reducer,
  middleware: [...redux.getDefaultMiddleware({immutableCheck: false})]
});