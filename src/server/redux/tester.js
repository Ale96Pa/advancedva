const store = require('./store');
const slice = require('./slice');
const request = require('request');

post = slice.crudSlicer.actions.post;

function dat(){
  request({uri: "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni.json"}, 
    async function(error, response, body) {

      // Parse JSON file with the whole data
      var obj = JSON.parse(body);
      for(var i=0; i<40; i++){
        store.store.dispatch(post(obj[i]));
      }
      //console.log(store.store.getState());
      const st = slice.selectorRegions(store.store.getState());
      //console.log(st);

      const abruzzo = slice.selectorRegionByName(store.store.getState(), "Abruzzo");
      console.log(abruzzo);
    });
    
}

dat();


