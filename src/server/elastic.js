'use strict'

const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: 'http://localhost:9200' })

// Query to delete an index and clean the dataset
async function deleteIndex(indexName){  
    const {body} = await client.indices.delete({
      index: indexName
  });
}

// Get all indices
async function getIndices(){
  var indices = [];
  const {body} = await client.cat.indices({
    format: "json"
  });
  for(var i=0; i<body.length; i++){
    indices.push(String(body[i].index));
  }
  console.log(indices);
}

// Query to insert data into an index
async function insertRegions(){

  const {body} = await client.indices.create({
    index: 'regions',
    body: {
      mappings: {
        properties: {
          denominazione_regione: { 
            type: "text",
            fielddata: true 
          }  
        }
      }
    }
  });

  // Get dataset from URL
  var request = require("request");
  request({uri: "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni.json"}, 
    async function(error, response, body) {

      // Parse JSON file with the whole data
      var obj = JSON.parse(body);
      var isPicked = false;
      var props = [];

      
      for(var i=0; i<(body.length)/2; i++){
        if (!isPicked){
          for (var prop in obj[i]){
            props.push(prop);
            isPicked = true;
          }
        }        
        // Insert data in index
        const {body} = await client.index({
          index: 'regions',
          body: {
              [props[0]]: obj[i][props[0]],
              [props[1]]: obj[i][props[1]],
              [props[2]]: obj[i][props[2]],
              [props[3]]: obj[i][props[3]],
              [props[4]]: obj[i][props[4]],
              [props[5]]: obj[i][props[5]],
              [props[6]]: obj[i][props[6]],
              [props[7]]: obj[i][props[7]],
              [props[8]]: obj[i][props[8]],
              [props[9]]: obj[i][props[9]],
              [props[10]]: obj[i][props[10]],
              [props[11]]: obj[i][props[11]],
              [props[12]]: obj[i][props[12]],
              [props[13]]: obj[i][props[13]],
              [props[14]]: obj[i][props[14]],
              [props[15]]: obj[i][props[15]],
              [props[16]]: obj[i][props[16]],
              [props[17]]: obj[i][props[17]],
              [props[18]]: obj[i][props[18]],
              [props[19]]: obj[i][props[19]],
              [props[20]]: obj[i][props[20]]
          }
        });
      }
    });
}

async function checkIndex(){
  const {body} = await client.search({
    index: 'regions',
    size: 10000,
  });

  console.log(body.hits.hits.length);
}

// Query to insert data into an index
async function aggregate(){
  const {body} = await client.search({
    index: 'regions',
    size: 10000,
    body: {
      aggs: {
        test_filter: {
          filter: {
            term: {
              denominazione_regione: "lazio"
            }
          },
          aggs: {
            avg_tamponi: { 
              avg: { 
                field: "tamponi" 
              } 
            }
          }
        }
      },
      aggs: {
        multi_filters: {
          filters: {
            filters: [
              {match: {
                denominazione_regione: "lazio"
              }},
              {match: {
                denominazione_regione: "abruzzo"
              }}
            ]
          }
        }
      }
    }
  });

  console.log(body.aggregations);
}

exports.getAggregation = async function getAggregration(nameRegion){
  const {body} = await client.search({
    index: 'regions',
    size: 10000,
    body:{
      aggs:{
        filter_region:{
          filters:{
            filters:[
              {match:{denominazione_regione: nameRegion}}
            ]
          }
        }
      }
    }
  });
}


//insertRegions();
//checkIndex();
//getIndices();
//deleteIndex("regions");
//aggregate();
this.getAggregation("Abruzzo");
