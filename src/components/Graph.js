import React, { Component } from "react";
import { TextField } from '../../node_modules/@material-ui/core';

const store = require('../server/redux/store');
const slice = require('../server/redux/slice');
const request = require('request');


class Graph extends Component{

  constructor(props){
    super(props);

    this.state = {
      region: "region",
      inputUser: "",
      regionData: [],
      totCases: []
    };

    this.printBtn = this.printBtn.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  
  handleChange(event){
    this.setState({inputUser: event.target.value});
  }

  printBtn(){
    const post = slice.crudSlicer.actions.post;
    const clear = slice.crudSlicer.actions.clear;

    request({uri: "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni.json"}, 
    async function(error, response, body) {

      // Parse JSON file with the whole data
      var obj = JSON.parse(body);
      for(var i=0; i<200; i++){
        store.store.dispatch(post(obj[i]));
      }

    });
    const selRegion = slice.selectorRegionByName(store.store.getState(), this.state.inputUser);
    if(selRegion.length > 0){
      this.setState({region: selRegion[0].denominazione_regione});
      this.setState({regionData: selRegion});

      var tot = [];
      for(var j=0; j<selRegion.length; j++){
        tot.push(selRegion[j].totale_casi);
        tot.push("\n-->\n");
      }
      this.setState({totCases: tot});
    } else {
      this.setState({region: "Invalid region"});
      this.setState({totCases: "NaN"});
    }
    store.store.dispatch(clear());
  }

  render(){
    return (
      <div>
        <form noValidate autoComplete="off">
          <TextField id="standard-basic" label="Region" onChange={this.handleChange}/>
        </form>
        <button onClick={() => this.printBtn()}>Button</button>
        <p>{this.state.totCases}</p>
      </div>
    )
  }
}

export default Graph;