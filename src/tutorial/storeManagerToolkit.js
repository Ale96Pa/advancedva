const redux = require('@reduxjs/toolkit');

/**
 * CONTAINER OF REDUCERS
 */
const counterSlice = redux.createSlice({
  name: 'counter',
  initialState: {
    value: 0
  },
  reducers: {
    incremented: state => {
      state.value += 1
    },
    decremented: state => {
      state.value -= 1
    }
  }
});

exports = { incremented, decremented } = counterSlice.actions

/**
 * STORE
 */
const store = redux.configureStore({
  reducer: counterSlice.reducer
})

store.subscribe(() => console.log(store.getState()))

store.dispatch(incremented())
store.dispatch(incremented())
store.dispatch(decremented())