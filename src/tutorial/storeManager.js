const redux = require('redux');

/**
 * REDUCER => signature: (state, action) => newState
 */
function counterReducer(state = { value: 0 }, action) {
  switch (action.type) {
    case 'counter/incremented':
      return { value: state.value + 1 }
    case 'counter/decremented':
      return { value: state.value - 1 }
    default:
      return state
  }
}

/**
 *  STORE => API:{ subscribe, dispatch, getState }.
 */ 
let store = redux.createStore(counterReducer)

// SUBSCRIBE or REACT-REDUX to update the store
store.subscribe(() => console.log(store.getState()))

// DISPATCH => update the store
store.dispatch({ type: 'counter/incremented' })
store.dispatch({ type: 'counter/incremented' })
store.dispatch({ type: 'counter/decremented' })