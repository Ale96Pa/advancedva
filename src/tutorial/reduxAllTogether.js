const redux = require('@reduxjs/toolkit');

/**
 * CONTAINER OF REDUCERS
 */
const crudSlicer = redux.createSlice({
  name: 'crud',
  initialState: {
      data: '',   
      stato: '',
      codice_regione: 0,
      denominazione_regione: '',
  },
  reducers: {
    post: (state, obj)=> {
      state.data = obj.payload.data;
    }
  }
});

exports = { post } = crudSlicer.actions

/**
 * STORE
 */
const store = redux.configureStore({
  reducer: crudSlicer.reducer
})

const newObj = {
  data: '2020-05-06T17:00:00',   
  stato: 'ITA',
  codice_regione: 12,
  denominazione_regione: 'Lazio',
}

//store.subscribe(() => console.log(store.getState()))

store.dispatch(post(newObj));

const selectorData = state => state.data;
const valData = selectorData(store.getState());
console.log(valData);

