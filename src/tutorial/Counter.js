/**
 * This file configure the UI to set the value as a state in the store
 */
import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import {
  decrement,
  increment,
  selectCount
} from './counterSlice'

export function Counter() {
  const count = useSelector(selectCount) // Extract value param from the state
  const dispatch = useDispatch()

  return (
    <div>
        <button onClick={() => dispatch(increment())}>
        +
        </button>

        <p>{count}</p>

        <button onClick={() => dispatch(decrement())}>
        -
        </button>
    </div>
  )
}