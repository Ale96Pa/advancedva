/**
 * This file contains the reducers: functions to get and update the state
 * A Slice is a container of more than one reducers
 */
import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
  name: 'counter',
  initialState: {
    value: 0
  },
  reducers: {
    increment: state => {
      state.value += 1
    },
    decrement: state => {
      state.value -= 1
    }
  }
})

// createlice automatically generates action creators with the same names as the reducer functions
export const { increment, decrement } = counterSlice.actions

// Export the reducer used to manage the store
export default counterSlice.reducer

// Selector extracts specific pieces of information from a store state.
export const selectCount = state => state.counter.value